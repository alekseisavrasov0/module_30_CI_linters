from datetime import datetime, timedelta
from typing import Generator

import pytest
from flask import Flask
from flask.testing import FlaskClient
from flask_sqlalchemy import SQLAlchemy

from module_30_CI_linters.flaskr.app import create_app
from module_30_CI_linters.flaskr.models import Client, ClientParking, Parking


@pytest.fixture
def app() -> Generator[Flask, None, None]:
    app = create_app(
        test_config={"TESTING": True, "SQLALCHEMY_DATABASE_URI": "sqlite://"}
    )

    with app.app_context():
        db: SQLAlchemy = app.extensions["sqlalchemy"].db
        db.create_all()

        try:
            client = Client(
                name="John",
                surname="Doe",
                credit_card="1234567890",
                car_number="ABC123",
            )
            parking = Parking(
                address="Test_Address",
                opened=True,
                count_places=10,
                count_available_places=10,
            )
            parking_client = ClientParking(
                client_id=1,
                parking_id=1,
                time_in=datetime(2023, 1, 1, 12, 0, 0) - timedelta(hours=1),
            )
            db.session.add_all([client, parking, parking_client])
            db.session.commit()

            yield app
        finally:
            db.session.rollback()
            db.session.close()
            db.drop_all()


@pytest.fixture
def test_data(db):
    # Создаем данные для клиента
    client_data = Client(
        name="test_name",
        surname="test_surname",
        credit_card="test_credit_card",
        car_number="test_car_number",
    )
    db.session.add(client_data)

    # Создаем данные для парковки
    parking_data = Parking(
        address="Test Parking",
        count_places=10,
        count_available_places=10,
    )
    db.session.add(parking_data)

    db.session.commit()

    return {"client": client_data, "parking": parking_data}


@pytest.fixture
def client(app: Flask) -> FlaskClient:
    return app.test_client()


@pytest.fixture
def db(app: Flask) -> SQLAlchemy:
    with app.app_context():
        return app.extensions["sqlalchemy"].db
