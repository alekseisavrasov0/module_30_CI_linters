from datetime import datetime, timedelta
from typing import Any, Dict, Optional

import pytest
from flask.testing import FlaskClient

from module_30_CI_linters.flaskr.models import Client, ClientParking, Parking


@pytest.mark.parametrize("endpoint", ["/clients"])
def test_get_endpoints_return_200(client: FlaskClient, endpoint: str):
    response = client.get(endpoint, method="GET")
    assert response.status_code == 200


def test_create_client(client: FlaskClient, db: Any):
    data: Dict[str, str] = {
        "name": "test_name",
        "surname": "test_surname",
        "credit_card": "test_credit_card",
        "car_number": "test_car_number",
    }
    response = client.post("/clients", json=data)
    assert response.status_code == 201

    client_in_db: Optional[Client] = (
        db.session.query(Client).filter_by(name="John").first()
    )
    assert client_in_db is not None


def test_create_parking(client: FlaskClient, db: Any):
    data: Dict[str, Any] = {"address": "Test Parking", "count_places": 10}
    response = client.post("/parkings", json=data)
    assert response.status_code == 201

    parking_in_db: Optional[Parking] = (
        db.session.query(Parking).filter_by(address="Test Parking").first()
    )
    assert parking_in_db is not None


@pytest.mark.parking
def test_check_in(client: FlaskClient, db: Any, test_data: Dict[str, Any]):
    client_data: Client = test_data["client"]
    parking_data: Parking = test_data["parking"]

    response_check_in = client.post(
        "/client_parking",
        json={"client_id": client_data.id, "parking_id": parking_data.id},
    )
    assert response_check_in.status_code == 201

    parking_in_db: Optional[Parking] = (
        db.session.query(Parking).filter_by(id=parking_data.id).first()
    )
    assert parking_in_db.count_available_places == parking_data.count_places - 1

    entry_in_db: Optional[ClientParking] = (
        db.session.query(ClientParking)
        .filter_by(client_id=client_data.id, parking_id=parking_data.id)
        .first()
    )
    assert entry_in_db is not None


@pytest.mark.parking
def test_check_out(client: FlaskClient, db: Any, test_data: Dict[str, Any]):
    client_data: Client = test_data["client"]
    parking_data: Parking = test_data["parking"]
    client_parking_data = ClientParking(
        client_id=client_data.id,
        parking_id=parking_data.id,
        time_in=datetime(2023, 1, 1, 12, 0, 0),
    )

    time_out = datetime.now() + timedelta(hours=1)

    db.session.add(client_data)
    db.session.add(parking_data)
    db.session.add(client_parking_data)
    db.session.commit()

    response_check_out = client.delete(
        "/client_parking",
        json={
            "client_id": client_data.id,
            "parking_id": parking_data.id,
            "time_out": time_out.isoformat(),
        },
    )
    assert response_check_out.status_code == 200

    parking_in_db: Optional[Parking] = (
        db.session.query(Parking).filter_by(id=parking_data.id).first()
    )
    assert parking_in_db.count_available_places == parking_data.count_available_places

    entry_in_db: Optional[ClientParking] = (
        db.session.query(ClientParking)
        .filter_by(client_id=client_data.id, parking_id=parking_data.id)
        .first()
    )
    assert entry_in_db is not None
    assert entry_in_db.time_out is not None
    assert entry_in_db.time_out <= time_out
