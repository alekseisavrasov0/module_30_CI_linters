import factory
from factory import LazyAttribute
from flask.testing import FlaskClient

from module_30_CI_linters.flaskr.models import Client, Parking, db


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    credit_card = factory.Faker("credit_card_number")
    car_number = factory.Faker("license_plate")


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker("address")
    opened = factory.Faker("boolean")
    count_places = factory.Faker("random_int", min=1, max=100)
    count_available_places = LazyAttribute(lambda o: o.count_places)


def test_create_client_with_factory(client: FlaskClient, db: db.Model):
    client_factory = ClientFactory()

    data: dict = {
        "name": client_factory.name,
        "surname": client_factory.surname,
        "credit_card": client_factory.credit_card,
        "car_number": client_factory.car_number,
    }

    response = client.post("/clients", json=data)
    assert response.status_code == 201

    client_in_db: Client = db.session.query(Client).filter_by(name=data["name"]).first()
    assert client_in_db is not None
    assert client_in_db.name == data["name"]


def test_create_parking_with_factory(client: FlaskClient, db: db.Model):
    parking_factory = ParkingFactory()

    data: dict = {
        "address": parking_factory.address,
        "opened": parking_factory.opened,
        "count_places": parking_factory.count_places,
        "count_available_places": parking_factory.count_available_places,
    }

    response = client.post("/parkings", json=data)
    assert response.status_code == 201

    parking_in_db: Parking = (
        db.session.query(Parking).filter_by(name=data["address"]).first()
    )
    assert parking_in_db is not None
    assert parking_in_db.address == data["address"]
