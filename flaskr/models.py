from typing import Any, Dict

from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()


class Client(db.Model):
    __tablename__ = "clients"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50), nullable=False)
    car_number = db.Column(db.String(10), nullable=False)

    def __init__(self, name, surname, credit_card, car_number):
        self.name = name
        self.surname = surname
        self.credit_card = credit_card
        self.car_number = car_number

    def __repr__(self):
        return f"Клиент {self.name}"

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Parking(db.Model):
    __tablename__ = "parkings"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)

    def __init__(self, address, count_places, count_available_places, opened=True):
        self.address = address
        self.opened = opened
        self.count_places = count_places
        self.count_available_places = count_available_places

    def __repr__(self):
        return f"Парковка {self.address}"

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ClientParking(db.Model):
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("clients.id"))
    parking_id = db.Column(db.Integer, db.ForeignKey("parkings.id"))
    time_in = db.Column(db.DateTime)
    time_out = db.Column(db.DateTime)

    def __init__(self, client_id, parking_id, time_in, time_out=None):
        self.client_id = client_id
        self.parking_id = parking_id
        self.time_in = time_in
        self.time_out = time_out

    client = db.relationship("Client", backref=db.backref("client_parkings", lazy=True))
    parking = db.relationship(
        "Parking", backref=db.backref("parking_clients", lazy=True)
    )

    __table_args__ = (
        db.UniqueConstraint("client_id", "parking_id", name="unique_client_parking"),
    )

    def to_json(self):
        return {
            "client_id": self.client_id,
            "parking_id": self.parking_id,
            "time_in": self.time_in.isoformat(),
            "time_out": self.time_out.isoformat() if self.time_out else None,
        }
