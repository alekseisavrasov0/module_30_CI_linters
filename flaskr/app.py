import os
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union

from flask import Flask, Response, jsonify, request

from module_30_CI_linters.flaskr.models import db


def create_app(test_config: Optional[Dict] = None) -> Flask:
    app: Flask = Flask(__name__, instance_relative_config=True)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///parking.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.before_request
    def before_request_func() -> None:
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/hello")
    def hello() -> str:
        return "Hello, World!"

    @app.route("/clients", methods=["GET"])
    def get_clients() -> Tuple[Response, int]:
        """Список всех клиентов"""
        clients: List[Client] = Client.query.all()
        clients_json: List[Dict[str, Union[str, int]]] = [
            client.to_json() for client in clients
        ]
        return jsonify({"clients": clients_json}), 200

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client_by_id(client_id: int) -> Tuple[Response, int]:
        """Информация клиента по ID"""
        client: Optional[Client] = Client.query.get(client_id)

        if client:
            return jsonify({"client": client.to_json()}), 200
        else:
            return jsonify({"message": "Client not found"}), 404

    @app.route("/clients", methods=["POST"])
    def create_client() -> Tuple[Response, int]:
        """Создать нового клиента"""
        data: Any = request.json
        new_client: Client = Client(
            name=data["name"],
            surname=data["surname"],
            credit_card=data.get("credit_card", ""),
            car_number=data.get("car_number", ""),
        )
        db.session.add(new_client)
        db.session.commit()
        return jsonify({"client": new_client.to_json()}), 201

    @app.route("/parkings", methods=["POST"])
    def create_parking() -> Tuple[Response, int]:
        """Создать новую парковочную зону"""
        data: Any = request.json
        new_parking: Parking = Parking(
            address=data["address"],
            opened=data.get("opened", True),
            count_places=data["count_places"],
            count_available_places=data["count_places"],
        )
        db.session.add(new_parking)
        db.session.commit()
        return jsonify({"parking": new_parking.to_json()}), 201

    @app.route("/client_parking", methods=["POST"])
    def check_in() -> Tuple[Response, int]:
        """Заезд на парковку"""
        data: Any = request.json
        client_id: Union[str, int, None] = data.get("client_id")
        parking_id: Union[str, int, None] = data.get("parking_id")

        client: Optional[Client] = Client.query.get(client_id)
        parking: Optional[Parking] = Parking.query.get(parking_id)

        if not client or not parking:
            return jsonify({"message": "Client or parking not found"}), 404

        if parking.count_available_places > 0:
            parking.count_available_places -= 1
            time_in: datetime = datetime.now()
            new_entry: ClientParking = ClientParking(
                client_id=client_id, parking_id=parking_id, time_in=time_in
            )
            db.session.add(new_entry)
            db.session.commit()
            return jsonify({"entry": new_entry.to_json()}), 201
        else:
            return jsonify({"message": "Parking is full"}), 400

    @app.route("/client_parking", methods=["DELETE"])
    def check_out() -> Tuple[Response, int]:
        """Выезд с парковки"""
        data: Any = request.json
        client_id: Optional[int] = data.get("client_id")
        parking_id: Optional[int] = data.get("parking_id")

        entry: Optional[ClientParking] = ClientParking.query.filter_by(
            client_id=client_id, parking_id=parking_id
        ).first()

        if not entry:
            return jsonify({"message": "Entry not found"}), 404

        time_out: datetime = datetime.now()
        entry.time_out = time_out
        parking: Optional[Parking] = Parking.query.get(parking_id)
        if parking and hasattr(parking, "count_available_places"):
            parking.count_available_places += 1

        db.session.commit()
        return jsonify({"message": "Check-out successful"}), 200

    return app
